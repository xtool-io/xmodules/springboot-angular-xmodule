version: "2.4"
services:
  ${projectName}-oraclexe:
    image: reg.tre-pa.jus.br/library/oracle-xe-11g-r2:latest
    container_name: ${projectName}-oraclexe
    volumes:
      - ./src/main/resources/docker/oraclexe:/docker-entrypoint-initdb.d
    environment:
      - ORACLE_DISABLE_ASYNCH_IO=true
      - ORACLE_ALLOW_REMOTE=true
    ports:
      - 1521:1521
    networks:
      - ${projectName}-net
  ${projectName}-keycloak:
    image: jboss/keycloak:13.0.1
    container_name: ${projectName}-keycloak13.0.1
    command: [ "-b", "0.0.0.0",
                "-Dkeycloak.migration.action=import",
                "-Dkeycloak.migration.provider=dir",
                "-Dkeycloak.migration.dir=/tmp/jboss/keycloak/realm-config",
                "-Dkeycloak.migration.strategy=IGNORE_EXISTING",
                "-Djboss.socket.binding.port-offset=5",
                "-Dkeycloak.profile.feature.upload_scripts=enabled" ]
    volumes:
      - ./src/main/resources/docker/keycloak/realm-config:/tmp/jboss/keycloak/realm-config
      - ~/apps01/kc11.0.1/db:/tmp/jboss/keycloak/standalone/data
    environment:
      - KEYCLOAK_USER=admin
      - KEYCLOAK_PASSWORD=admin
      - DB_VENDOR=h2
      - KEYCLOAK_IMPORT=/tmp/jboss/keycloak/realm-config/TRE-PA-realm.json
    ports:
      - 8085:8085
      - 8448:8448
      - 9995:9995
networks:
  ${projectName}-net:
    name: ${projectName}-net
