<#assign schemaName=projectName?replace('_', '')?truncate(30, '') >
--**********************************
--Ajuste do NLS_CHARACTERSET
--**********************************
connect sys/oracle as sysdba;
shutdown;
startup restrict;
Alter database character set INTERNAL_USE WE8ISO8859P1;
shutdown immediate;
startup;
connect system/oracle

--**********************************
--Tuning OracleXE
--**********************************
alter system set filesystemio_options=directio scope=spfile;
alter system set disk_asynch_io=false scope=spfile;

--**********************************
--Esquema ${schemaName}
--**********************************

create tablespace ${schemaName} datafile '/u01/app/oracle/oradata/XE/${schemaName}01.dbf' size 100M online;
create tablespace idx_${schemaName} datafile '/u01/app/oracle/oradata/XE/idx_${schemaName}01.dbf' size 100M;
create user ${schemaName} identified by ${schemaName} default tablespace ${schemaName} temporary tablespace temp;
grant resource to ${schemaName};
grant connect to ${schemaName};
grant create view to ${schemaName};
grant create procedure to ${schemaName};
grant create materialized view to ${schemaName};
alter user ${schemaName} default role connect, resource;
exit;
