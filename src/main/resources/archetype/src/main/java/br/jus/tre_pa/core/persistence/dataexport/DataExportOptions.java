package br.jus.tre_pa.core.persistence.dataexport;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe com as opções de exportação.
 */
@Getter
@Setter
@ToString
public class DataExportOptions {

    public enum ExportType {
        CSV,
        PDF,
        XLSX,
        JSON
    }

    private ExportType exportType = ExportType.CSV;

    private List<String> columns = new ArrayList<>();

    private String fileName;

}
