package br.jus.tre_pa.core.persistence.dataexport;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * Classe com os dados do resultado da exportação.
 */
@Getter
@Setter
@ToString
public class DataExportResult {

    private ByteArrayResource content;

    private HttpHeaders httpHeaders;

    public ResponseEntity<Resource> toResponseEntity() {
        return ResponseEntity
            .ok()
            .contentType(MediaType.APPLICATION_OCTET_STREAM)
            .headers(this.getHttpHeaders())
            .contentLength(this.getContent().contentLength())
            .body(this.getContent());
    }

}
