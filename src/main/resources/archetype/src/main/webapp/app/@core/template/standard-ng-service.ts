import {Observable, throwError} from "rxjs";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {LoadOptions} from "devextreme/data/load_options";
import {HttpParamsAdapter} from "../types/http-params-adapter";
import {Page} from "../types";
import {catchError, map} from "rxjs/operators";
import {DataExportOptions} from "../dataexport/data-export-options";
import {saveAs} from 'file-saver';

export abstract class StandardNgService {

  abstract URL_API: string;

  protected constructor(protected http: HttpClient) {
  }

  public findAll<T>(loadOptions: LoadOptions): Observable<{ data: T[]; totalCount: number }> {
    const params = new HttpParamsAdapter(loadOptions).httpParams();
    return this.http.get<Page<T>>(this.URL_API, {params})
      .pipe(
        map((page: Page<T>) => ({
          data: page.content,
          totalCount: page.totalElements
        })),
        catchError(error => {
          return throwError(new Error(error.message))
        })
      )
  }

  public findById<T, ID>(id: ID): Observable<T> {
    return this.http.get<T>(`${this.URL_API}/${id}`);
  }

  public save<T>(resource: T): Observable<T> {
    return resource['id'] ? this.update(resource['id'], resource) : this.insert(resource);
  }

  public insert<T>(resource: T): Observable<T> {
    return this.http.post<T>(this.URL_API, resource);
  }

  public update<T, ID>(id: ID, resource: T): Observable<T> {
    return this.http.put<T>(`${this.URL_API}/${id}`, resource);
  }

  public patch<ID>(id: ID, resourceValues: any) {
    return this.http.patch(`${this.URL_API}/${id}`, resourceValues);
  }

  public delete<ID>(id: ID): Observable<void> {
    return this.http.delete<void>(`${this.URL_API}/${id}`);
  }


  public export(loadOptions: LoadOptions, dataExportOptions: DataExportOptions) {
    console.log("LoadOption: ", loadOptions)
    const params = new HttpParamsAdapter(loadOptions).httpParams();
    return this.http.post(`${this.URL_API}/export`, dataExportOptions,
      {
        responseType: 'blob',
        observe: 'response',
        params: params
      })
      .subscribe(httpResponse =>
        saveAs(
          httpResponse.body,
          this.getFilenameFromHttpResponse(httpResponse))
      )
  }

  private getFilenameFromHttpResponse(httpResponse: HttpResponse<any>): string {
    const contentDisposition = httpResponse.headers.get('content-disposition');
    return contentDisposition.split(';')[1].split('filename')[1].split('=')[1].trim()
      .replace("\"", "")
      .replace("\"", "");
  }
}
