package xmodule.sbngxmod

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringbootAngularXmoduleApplication

fun main(args: Array<String>) {
    SpringApplicationBuilder(SpringbootAngularXmoduleApplication::class.java)
        .bannerMode(Banner.Mode.OFF)
        .logStartupInfo(false)
        .headless(false)
        .run(*args)
}
