package xmodule.sbngxmod.initializer

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import picocli.CommandLine
import picocli.CommandLine.Command
import xdeps.xdepfs.context.CopyFileContext
import xdeps.xdepfs.tasks.FileSystemTask
import xdeps.xdepgit.tasks.GitTask
import xmodule.core.command.AbstractInitializer
import xmodule.core.context.WorkspaceContext
import xmodule.core.utils.echo
import xmodule.core.utils.header

@Component
@Command(name = "init")
class AppInitializer(
    private val workspaceContext: WorkspaceContext,
    private val fsTask: FileSystemTask,
    private val gitTask: GitTask
) : AbstractInitializer() {

    private val log = LoggerFactory.getLogger(AppInitializer::class.java)

    @CommandLine.Option(names = ["--description"], description = ["Descrição do projeto"], required = true)
    lateinit var projectDescription: String
    override fun run() {
        header("Copiando arquivos do projeto")
        fsTask.copyFiles(
            classpathSource = "archetype",
            destination = workspaceContext.path,
            vars = mapOf(
                "projectName" to workspaceContext.basename,
                "projectDesc" to projectDescription
            ),
            onEachCreated = { ctx: CopyFileContext -> log.info("${ctx.path} (${ctx.byteCountToDisplaySize})") }
        )
//        header("Baixando dependências do frontend")
//        npmTask.install()
        header("Inicializando o projeto no git")
        gitTask.init()
        gitTask.add().apply {
            log.info("${this?.entryCount} arquivos adicionados ao repositório git.")
        }
        header("Instruções gerais")
        echo(
            """
          Aplicação @|blue ${workspaceContext.basename}|@ gerada com sucesso! Siga os procedimentos abaixo para rodar localmente:
          
          @|green 1.|@ Iniciando os serviços do OracleXE e Keycloak:
            ${'$'} @|bold sudo docker-compose up |@ (Em algumas versões do docker o comando pode ser: ${'$'} @|bold sudo docker compose up|@)
          
          @|green 2.|@ Iniciando o frontend:
            ${'$'} @|bold npx ng s |@
          
          @|green 3.|@ Iniciando o backend:
            ${'$'} @|bold mvn clean spring-boot:run -s .m2/settings.xml|@
          
          -----
          
          Informações adicionais:
          
          @|bold Aplicação |@ => http://localhost:4200 (root/root)
          @|bold Keycloak |@  => http://localhost:8085 (admin/admin)
          @|bold OracleXE |@  => Ver arquivo application.yml
        """.trimIndent()
        )

//        shellTask.runCommand(
//            command = """
//                git init &&
//                git add . &&
//                git commit -m "First Commit"
//            """.trimIndent()
//        )
    }
}
