package xmodule.sbngxmod.command

import org.springframework.stereotype.Component
import picocli.CommandLine.Command
import xmodule.core.command.AbstractCommand

@Component
@Command(name = "repository")
class RepositoryCommand: AbstractCommand() {
    override fun run() {
        println("Em RepositoryCommand")
    }
}
